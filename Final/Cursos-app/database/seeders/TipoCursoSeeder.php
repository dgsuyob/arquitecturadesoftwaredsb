<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TipoCurso;

class TipoCursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TipoCurso::create(['id'=>1,'nombre'=>'Seminarios']);
        TipoCurso::create(['id'=>2,'nombre'=>'Formacion Continua']);
        TipoCurso::create(['id'=>3,'nombre'=>'Talleres']);
    }
}
