<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\TipoCurso;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Curso>
 */
class CursoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre' => $this->faker->name(),
            'fecha_inicio' => $this->faker->date(),
            'fecha_final' => $this->faker->date(),
            'tipo_curso_id' => TipoCurso::inRandomOrder()->first()->id,
        ];
    }
}
