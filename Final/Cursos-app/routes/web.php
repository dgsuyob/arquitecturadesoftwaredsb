<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\TipoCursoController;
use App\Models\TipoCurso;

Route::resource('cursos', CursoController::class);
Route::resource('tipoCursos', TipoCursoController::class)->parameters([
    'tipoCursos' => 'tipoCurso'
]);
Route::get('/', [CursoController::class, 'index'])->name('home');
