<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear Curso</title>

</head>
<body>
    <h1>Crear Nuevo Curso</h1>

    <form action="{{ route('cursos.store') }}" method="POST">
        @csrf

            <label for="nombre">Nombre del Curso:</label>
            <input type="text" id="nombre" name="nombre" required>
            <br>
            <label for="fecha_inicio">Fecha de Inicio:</label>
            <input type="date" id="fecha_inicio" name="fecha_inicio" required>
            <br>
            <label for="fecha_final">Fecha de Finalización:</label>
            <input type="date" id="fecha_final" name="fecha_final" required>
            <br>

        <div>
            <label for="tipo_curso_id">Tipo de Curso:</label>
            <select id="tipo_curso_id" name="tipo_curso_id" required>
                <option value="" disabled selected>Selecciona un tipo de curso</option>
                @foreach($tipoCurso as $tipo)
                    <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                @endforeach
            </select>
        </div>

        <button type="submit">Crear Curso</button>
    </form>
</body>
</html>
