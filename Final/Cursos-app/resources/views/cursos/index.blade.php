<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Cursos</title>
    <style>
        body {
            padding: 20px;
        }

        h1 {
            color: #333;
        }

        table {
            width: 50%;
            border-collapse: collapse;
        }

        th,
        td {
            border: 1px solid black;
            text-align: left;
        }

    </style>
</head>

<body>
    


    <table>
    <h1>Lista de Cursos</h1>
    <br>
    <a href="{{ route('cursos.create') }}">Crear Nuevo Curso</a>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Fecha Inicio</th>
                <th>Fecha Final</th>
                <th>Tipo de Curso</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($cursos as $curso)
            <tr>
                <td>{{ $curso->id }}</td>
                <td>{{ $curso->nombre }}</td>
                <td>{{ $curso->fecha_inicio }}</td>
                <td>{{ $curso->fecha_final }}</td>
                <td>{{ $curso->tipoCurso->nombre }}</td>
                <td class="action-buttons">
                    <a href="{{ route('cursos.edit', $curso->id) }}">Editar</a>
                    <form action="{{ route('cursos.destroy', $curso->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Eliminar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
