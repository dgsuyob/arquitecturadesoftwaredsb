<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Curso</title>
</head>

<body>
    <h1>Editar Curso</h1>

    <form action="{{ route('cursos.update', $curso->id) }}" method="POST">
        @csrf
        @method('PUT')
        <label for="nombre">Nombre</label>
        <input type="text" id="nombre" name="nombre" value="{{ $curso->nombre }}" required>

        <label for="fecha_inicio">Fecha Inicio</label>
        <input type="date" id="fecha_inicio" name="fecha_inicio" value="{{ $curso->fecha_inicio }}" required>

        <label for="fecha_final">Fecha Final</label>
        <input type="date" id="fecha_final" name="fecha_final" value="{{ $curso->fecha_final }}" required>

        <label for="tipo_curso_id">Tipo de Curso</label>
        <select id="tipo_curso_id" name="tipo_curso_id" required>
            @foreach($tipoCurso as $tipo)
                <option value="{{ $tipo->id }}" {{ $curso->tipo_curso_id == $tipo->id ? 'selected' : '' }}>{{ $tipo->nombre }}</option>
            @endforeach
        </select>

        <button type="submit">Actualizar</button>
    </form>
</body>

</html>
