<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;
    protected $fillable=['nombre','fecha_inicio','fecha_final','tipo_curso_id'];
    public function tipoCurso()
    {
        //relacion con el modelo ciudad          estandar
        return $this->belongsTo(TipoCurso::class, 'tipo_curso_id','id');
    }
}
