<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TipoCurso;

class CursoController extends Controller
{

    public function index()
    {
        $cursos = Curso::with('tipoCurso')->get();
        return view('cursos.index', ['cursos' => $cursos]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $tipoCurso = TipoCurso::all();
        return view('cursos.create', ['tipoCurso' => $tipoCurso])->with('success', 'curso creadoq correctamente.');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //depurar y ver valores
        // dd($request->all());
        Curso::create($request->all());
        return redirect()->route('cursos.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Curso $curso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Curso $curso)
    {
        // Muestra los valores del objeto
        // dd($participante);
        $tipoCurso = TipoCurso::all();
        return view('cursos.edit', ['curso' => $curso], ['tipoCurso' => $tipoCurso]);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Curso $curso)
    {
        $curso->update($request->all());
        return redirect()->route('cursos.index')->with('success', 'curso actualizado correctamente.');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Curso $curso)
    {
        $curso->delete();

        return redirect()->route('cursos.index')->with('success', 'curso eliminado correctamente.');
    }
}
