/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursosfactory;

import java.util.ArrayList;

/**
 *
 * @author david
 */
class Taller extends Curso {

    private ArrayList<String> listaActividades = new ArrayList();

    public Taller(String nombre, String fechaInicio, String fechaFinal) {
        super(nombre, fechaInicio, fechaFinal);
        this.listaActividades = new ArrayList<>();
    }

    public void agregarActividad(String actividad) {
        listaActividades.add(actividad);
    }

}
