/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursosfactory;

/**
 *
 * @author david
 */
class FabricaCursos {

    public static Curso crearCurso(String tipo, String nombre, String fechaInicio, String fechaFinal) {
        switch (tipo) {
            case "seminario":
                return new Seminario(nombre, fechaInicio, fechaFinal);
            case "formacioncontinua":
                return new FormacionContinua(nombre, fechaInicio, fechaFinal);
            case "taller":
                return new Taller(nombre, fechaInicio, fechaFinal);
            default:
                throw new IllegalArgumentException("Tipo de curso no válido");
        }
    }
}
