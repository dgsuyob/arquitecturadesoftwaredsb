/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursosfactory;

/**
 *
 * @author david
 */
public class Curso {

    private String nombre;
    private String fechaInicio;
    private String fechaFinal;

    public Curso(String nombre, String fechaInicio, String fechaFinal) {
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.fechaFinal = fechaFinal;
    }

}
