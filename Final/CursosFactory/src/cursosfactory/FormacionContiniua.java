/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursosfactory;

import java.util.ArrayList;

/**
 *
 * @author david
 */
class FormacionContinua extends Curso {

    private ArrayList<String> listaTemas = new ArrayList();

    public FormacionContinua(String nombre, String fechaInicio, String fechaFinal) {
        super(nombre, fechaInicio, fechaFinal);
        this.listaTemas = new ArrayList<>();
    }

    public void agregarTema(String tema) {
        listaTemas.add(tema);
    }

}
