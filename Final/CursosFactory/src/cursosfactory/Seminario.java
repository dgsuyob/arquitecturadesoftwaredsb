/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursosfactory;

import java.util.ArrayList;

/**
 *
 * @author david
 */
public class Seminario extends Curso {

    private ArrayList<String> grupoParticipantes = new ArrayList();

    public Seminario(String nombre, String fechaInicio, String fechaFinal) {
        super(nombre, fechaInicio, fechaFinal);
        this.grupoParticipantes = new ArrayList<>();
    }

    public void agregarParticipante(String participante) {
        grupoParticipantes.add(participante);
    }

}
