/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package cursosfactory;

/**
 *
 * @author david
 */
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("\n--- Menu de Creacion de Cursos ---");
            System.out.println("1. Crear Seminario");
            System.out.println("2. Crear Formación Continua");
            System.out.println("3. Crear Taller");
            System.out.println("4. Salir");
            System.out.print("Seleccione una opcion: ");
            int opcion = 0;
            opcion = scanner.nextInt();

            if (opcion == 4) {
                System.out.println("Hasta luego");
                break;
            }

            scanner.nextLine();

            switch (opcion) {
                case 1:
                    System.out.print("Nombre del curso: ");
                    String nombre = scanner.nextLine();
                    System.out.print("Fecha de inicio: ");
                    String fechaInicio = scanner.nextLine();
                    System.out.print("Fecha final: ");
                    String fechaFinal = scanner.nextLine();
                    Seminario seminario = (Seminario) FabricaCursos.crearCurso("seminario", nombre, fechaInicio, fechaFinal);
                    System.out.print("Nombre del participante: ");
                    String participante = scanner.nextLine();
                    seminario.agregarParticipante(participante);
                    System.out.println("Seminario creado exitosamente.");
                    break;
                case 2:
                    System.out.print("Nombre del curso: ");
                    nombre = scanner.nextLine();
                    System.out.print("Fecha de inicio: ");
                    fechaInicio = scanner.nextLine();
                    System.out.print("Fecha final: ");
                    fechaFinal = scanner.nextLine();
                    FormacionContinua formacionContinua = (FormacionContinua) FabricaCursos.crearCurso("formacioncontinua", nombre, fechaInicio, fechaFinal);
                    System.out.print("Tema a agregar: ");
                    String tema = scanner.nextLine();
                    formacionContinua.agregarTema(tema);
                    System.out.println("Formación continua creada exitosamente.");
                    break;
                case 3:
                    System.out.print("Nombre del curso: ");
                    nombre = scanner.nextLine();

                    System.out.print("Fecha de inicio: ");
                    fechaInicio = scanner.nextLine();

                    System.out.print("Fecha final: ");
                    fechaFinal = scanner.nextLine();
                    Taller taller = (Taller) FabricaCursos.crearCurso("taller", nombre, fechaInicio, fechaFinal);
                    System.out.print("Actividad a agregar: ");
                    String actividad = scanner.nextLine();
                    taller.agregarActividad(actividad);
                    System.out.println("Taller creado exitosamente.");
                    break;
                default:
                    System.out.println("Opcion no valida. Por favor, intente de nuevo.");
                    break;
            }
        }
    }
}
