/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package fabricaabstracta;

/**
 *
 * @author david
 */


   interface Forma { // Interfaz base para las formas
    void dibujar();
}

class Circulo implements Forma { // Implementación para círculo
    @Override
    public void dibujar() {
        System.out.println("Dibujando círculo");
    }
}

class Rectangulo implements Forma { // Implementación para rectángulo
    @Override
    public void dibujar() {
        System.out.println("Dibujando rectángulo");
    }
}

interface Color { // Interfaz base para los colores
    void rellenar();
}

class Rojo implements Color { // Implementación para color rojo
    @Override
    public void rellenar() {
        System.out.println("Relleno rojo");
    }
}

class Verde implements Color { // Implementación para color verde
    @Override
    public void rellenar() {
        System.out.println("Relleno verde");
    }
}

abstract class FabricaAbstracta { // Fábrica abstracta para crear formas y colores
    abstract Forma getForma(String tipoForma);
    abstract Color getColor(String tipoColor);
}

class FabricaBasica extends FabricaAbstracta { // Fábrica que crea formas y colores básicos

    @Override
    public Forma getForma(String tipoForma) {
        switch (tipoForma) {
            case "Circulo":
                return new Circulo();
            case "Rectangulo":
                return new Rectangulo();
            default:
                throw new IllegalArgumentException("Tipo de forma no válido");
        }
    }

    @Override
    public Color getColor(String tipoColor) {
        switch (tipoColor) {
            case "Rojo":
                return new Rojo();
            case "Verde":
                return new Verde();
            default:
                throw new IllegalArgumentException("Tipo de color no válido");
        }
    }
}

public class Cliente { // Cliente que utiliza la fábrica para crear formas y colores

    public static void main(String[] args) {
        FabricaAbstracta fabrica = new FabricaBasica();

        Forma circulo = fabrica.getForma("Circulo");
        circulo.dibujar(); // Dibujando círculo

        Color verde = fabrica.getColor("Verde");
        verde.rellenar(); // Relleno verde
    }
}

    

