@extends('layout.main')
@section('content')
<style>
    body{
        width: 100%;
        display: flex;
        flex-direction: column;
    }
    table {
        border-collapse: collapse;
        width: 60%;
        margin-bottom: 10px;
    }

    td,
    th {
        border: 1px solid #666666;
        padding: 2px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    tr:hover {
        background-color: #dddddd;
    }

    a {
        display: inline-block;
        padding: 8px 16px;
        color: #333333;
        text-decoration: none;
        border: 1px solid #333333;
        border-radius: 5px;
        transition: background-color 0.3s, color 0.3s;
    }

    a:hover {
        background-color: #333;
        color: #ffffff;
    }

    button {
        padding: 8px 16px;
        color: #ffffff;
        background-color: #333;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s, color 0.3s;
    }

    button:hover {
        background-color: #666666;
    }
</style>

<body>
    <div>
    <table>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Operación</th>
        </tr>
        @foreach ($editoriales as $editorial)
        <tr>
            <td>{{$editorial->id}}</td>
            <td>{{$editorial->nombre}}</td>
            <td>
                <a href="{{ route('editoriales.edit', $editorial->id) }}">Editar</a>
                <form action="{{ route('editoriales.destroy', $editorial->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" onclick="return confirm('¿Estás seguro de que deseas eliminar este participante?')">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    </div>
    <div>
    <a href="{{ route('editoriales.create') }}">Crear nueva ciudad</a>
    </div>
</body>
@endsection
