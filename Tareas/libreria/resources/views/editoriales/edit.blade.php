@extends('layout.main')

@section('content')
    <h1>Editar editorial</h1>

    <form action="{{ route('editoriales.update', $editorial->id) }}" method="POST">
        @csrf
        @method('PUT')

        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" id="nombre" value="{{ $editorial->nombre }}">
        <br>

        <button type="submit">Actualizar editorial</button>
    </form>
@endsection
