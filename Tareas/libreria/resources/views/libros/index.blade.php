@extends('layout.main')

@section('content')
<style>
    body{
        display: flex;
        flex-direction: column;
    }
    table {
        border-collapse: collapse;
        width: 80%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        padding: 8px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    tr:hover {
        background-color: #ddd;
    }

    a {
        display: inline-block;
        padding: 8px 16px;
        color: #333;
        text-decoration: none;
        border: 1px solid #333;
        border-radius: 5px;
        transition: background-color 0.3s, color 0.3s;
    }

    a:hover {
        background-color: #333;
        color: #fff;
    }

    button {
        padding: 8px 16px;
        color: #fff;
        background-color: #333;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s, color 0.3s;
    }

    button:hover {
        background-color: #666;
    }
</style>

<body>
    <div>
    <table>
        <tr>
            <th>Id</th>
            <th>Titulo</th>
            <th>Editorial</th>
            <th>Edicion</th>
            <th>Pais</th>
            <th>Precio</th>
            <th>Operación</th>
        </tr>
        @foreach ($libros as $libro)
        <tr>
            <td>{{$libro->id}}</td>
            <td>{{$libro->titulo}}</td>
            <td>
                @if ($libro->editorial)
                {{$libro->editorial->nombre}}
                @else
                Not Available
                @endif
            </td>
            <td>{{$libro->edicion}}</td>
            <td>{{$libro->pais}}</td>
            <td>{{$libro->precio}}</td>

            <td>
                <a href="{{ route('libros.edit', $libro->id) }}">Editar</a>
                <form action="{{ route('libros.destroy', $libro->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" onclick="return confirm('¿Estás seguro de que deseas eliminar este participante?')">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    </div>
    <div>
    <a href="{{ route('libros.create') }}">Crear nuevo participante</a>
    </div>
</body>
@endsection
