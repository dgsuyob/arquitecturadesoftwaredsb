@extends('layout.main')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="{{ route('libros.store') }}" method="post">
        @csrf
        <label for="titulo">Titulo</label>
        <input type="text" name="titulo">
        <br>
        <label for="editorial">Editorial</label>
        <select name="editorial_id" id="editorial">
            @foreach ($editoriales as $editorial)
            <option value="{{ $editorial->id }}">{{ $editorial->nombre }}</option>
            @endforeach
        </select>
        <br>
        <label for="edicion">Edicion</label>
        <input type="number" name="edicion" id="edicion">
        <br>

        <label for="pais">Pais</label>
        <input type="text" name="pais" id="pais">
        <br>
        <label for="precio">Precio</label>
        <input type="number" name="precio" id="precio">
        <br>
        
        <button type="submit">Guardar</button>
    </form>
@endsection