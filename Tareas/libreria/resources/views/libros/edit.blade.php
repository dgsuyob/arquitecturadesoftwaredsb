@extends('layout.main')

@section('content')
    <h1>Editar Libro</h1>

    <form action="{{ route('libros.update', $libro->id) }}" method="POST">
        @csrf
        @method('PUT')

        <label for="Titulo">Titulo:</label>
        <input type="text" value="{{ $libro->titulo }}" name="titulo" id="titulo">
        <br>

        <label for="editorial">Editorial:</label>
        <select name="editorial_id" id="editorial">
            @foreach ($editoriales as $editorial)
            <option value="{{ $editorial->id }}" @selected($editorial->id==$libro->editorial_id) >{{$editorial->nombre}}</option>
            @endforeach
        </select>   
        <br>

        <label for="edicion">Edicion:</label>
        <input type="number" value="{{ $libro->edicion }}"name="edicion" id="edicion">
        <br>

        <label for="pais">Pais:</label>
        <input type="text"  value="{{ $libro->pais }}"name="pais" id="pais">
        <br>

        <label for="precio">Precio:</label>
        <input type="number"  value="{{ $libro->precio }}"name="precio" id="precio">
        <br>
        
        <button type="submit">Actualizar libro</button>
    </form>
@endsection