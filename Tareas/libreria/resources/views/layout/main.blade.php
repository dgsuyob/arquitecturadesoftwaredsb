<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .container {
            display: flex;
            height: 100vh;
        }

        .sidebar {
            width: 20%;
            background-color: #343a40;
            height: 100vh;
            padding: 20px 0;

        }

        .sidebar h2 {
            color: #f8f9fa;
            padding: 0 20px;
            margin-bottom: 20px;
        }

        .sidebar ul {
            list-style-type: none;
            padding: 0;
        }

        .sidebar ul li a {
            display: block;
            padding: 10px 20px;
            color: #f8f9fa;
            text-decoration: none;
        }

        .sidebar ul li a:hover {
            background-color: #495057;
        }

        .main-content {
            width: 100%;
            padding-left: 50px;
            box-sizing: border-box;
            justify-content: center;

        }
    </style>
</head>

<body>
    <div class="container">
        <div class="sidebar">
            <h2>Menú</h2>
            <ul>
                <li><a href="libros">libros</a></li>
                <li><a href="editoriales">editoriales</a></li>
            </ul>
        </div>
        <div class="main-content">
            @yield('content')
        </div>
    </div>
</body>

</html>
