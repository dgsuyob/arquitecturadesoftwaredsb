<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EditorialController;
use App\Http\Controllers\LibroController;

Route::resource('libros', LibroController::class);
Route::resource('editoriales', EditorialController::class)->parameters([
    'editoriales' => 'editorial'
]);
Route::view('/', 'layout.main')->name('home');
