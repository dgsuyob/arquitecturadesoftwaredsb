<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Libro;
class Editorial extends Model
{
    use HasFactory;
    protected $table = 'editorials';
    protected $fillable = ['id', 'nombre'];
    public function libros()
    {
        return $this->hasMany(Libro::class);
    }
}
