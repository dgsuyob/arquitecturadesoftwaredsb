<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use App\Http\Controllers\Controller;
use App\Models\Editorial;
use Illuminate\Http\Request;

class LibroController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $libros = Libro::with('editorial')->get();
        return view('libros.index', ['libros' => $libros]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $editoriales = Editorial::all();
        return view('libros.create', ['editoriales' => $editoriales]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //depurar y ver valores
        // dd($request->all());
        Libro::create($request->all());
        return redirect()->route('libros.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Libro $libro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Libro $libro)
    {
        // Muestra los valores del objeto
        $editoriales = Editorial::all();
        return view('libros.edit', ['libro' => $libro], ['editoriales' => $editoriales]);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Libro $libro)
    {
       $libro->update($request->all());
        return redirect()->route('libros.index');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Libro $libro)
    {
        // Elimina el participante de la base de datos
        $libro->delete();

        // Redirecciona a la página index con un mensaje de éxito
        return redirect()->route('libros.index')->with('success', 'libro eliminado correctamente.');
    }
}
