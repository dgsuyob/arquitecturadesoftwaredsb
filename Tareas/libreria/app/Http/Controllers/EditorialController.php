<?php

namespace App\Http\Controllers;

use App\Models\Editorial;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EditorialController extends Controller
{
    public function index()
    {
        $editoriales = Editorial::all();
        return view('editoriales.index',['editoriales'=>$editoriales]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('editoriales.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Editorial::create($request->all());
        return redirect()->route('editoriales.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Editorial $editorial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Editorial $editorial)
    {
        return view('editoriales.edit',['editorial'=>$editorial]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Editorial $editorial)
    {
        $editorial->update($request->all());
        return redirect()->route('editoriales.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Editorial $editorial)
    {
        $editorial->delete();
        return redirect()->route('editoriales.index'); 
    }
}
