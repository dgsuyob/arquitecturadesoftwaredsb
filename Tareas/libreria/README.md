# correr laravel
se empieza por el web, por la route, luego por el controlador y las vistas

- php artisan serve 

#crear un modelo con su migracion, seede, factory, controlador y recursos
#en laravel los modelos deben empezr con mayusculas y estaar en singular

php artisan make:model Participante -msfcr

$ hacer correr las migraciones

php artisan migrate

#hacer correr las migraciones borrando todo

php artisan migrate:fresh

#hcer corre los seeders

$todos los seeders

-php artisan db:seed

#un solo seeder

-php artisan db:seed --class=PersonaSeeder

#hacer correr las migraciones borrando todo y hce correr los seeder

php artisan migrate:fresh --seed

#mostrar todas las rutas disponibles

php artisan route:list