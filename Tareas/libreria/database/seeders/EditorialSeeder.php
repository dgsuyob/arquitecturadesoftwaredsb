<?php

namespace Database\Seeders;

use App\Models\Editorial;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Editorial::create(['id'=>1,'nombre'=>'colombia']);
        Editorial::create(['id'=>2,'nombre'=>'astillero']);
        Editorial::create(['id'=>3,'nombre'=>'viajero']);
        Editorial::create(['id'=>4,'nombre'=>'astronauta']);
        Editorial::create(['id'=>5,'nombre'=>'el comercio']);

    }
}
