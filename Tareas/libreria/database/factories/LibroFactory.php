<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Editorial;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Libro>
 */
class LibroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "titulo" => $this->faker->unique()->word(),
            "editorial_id" => Editorial::all()->random()->id,
            "edicion" => $this->faker->unique()->numberBetween(1,100),
            "pais" => $this->faker->country(),
            "precio" => $this->faker->numberBetween(1,100)
        ];
        
    }
}
