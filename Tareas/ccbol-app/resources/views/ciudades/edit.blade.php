@extends('layout.main')

@section('content')
    <h1>Editar Ciudad</h1>

    <form action="{{ route('ciudades.update', $ciudad->id) }}" method="POST">
        @csrf
        @method('PUT')

        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" id="nombre" value="{{ $ciudad->nombre }}">
        <br>

        <button type="submit">Actualizar Ciudad</button>
    </form>
@endsection
