@extends('layout.main')

@section('content')
    <h1>Editar Participante</h1>

    <form action="{{ route('participantes.update', $participante->id) }}" method="POST">
        @csrf
        @method('PUT')

        <label for="nombres">Nombres:</label>
        <input type="text" name="nombres" id="nombres" value="{{ $participante->nombres }}">
        <br>

        <label for="apellidos">Apellidos:</label>
        <input type="text" name="apellidos" id="apellidos" value="{{ $participante->apellidos }}">
        <br>

        <label>Sexo:</label>
        <input type="radio" name="sexo" id="F" value="F" @checked($participante->sexo=='F')>
        <label for="F">F</label>
        <input type="radio" name="sexo" id="M" value="M" @checked($participante->sexo=='M')>
        <label for="M">M</label>
        <br>

        <label for="direccion">Dirección:</label>
        <input type="text" name="direccion" id="direccion" value="{{ $participante->direccion }}">
        <br>

        <label for="correo">Correo Electrónico:</label>
        <input type="email" name="correo" id="correo" value="{{ $participante->correo }}">
        <br>

        <label for="celular">Celular:</label>
        <input type="text" name="celular" id="celular" value="{{ $participante->celular }}">
        <br>

        <label for="ciudad">Ciudad:</label>
        <select name="ciudad_id" id="ciudad">
            @foreach ($ciudades as $ciudad)
            <option value="{{ $ciudad->id }}" @selected($ciudad->id==$participante->ciudad_id) >{{$ciudad->nombre}}</option>
            @endforeach
        </select>   
        <br>

        <button type="submit">Actualizar Participante</button>
    </form>
@endsection