<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="{{ route('participantes.store') }}" method="post">
        @csrf
        <label for="carnet_identidad">Carnet de identidad</label>
        <input type="text" name="carnet_identidad">
        <br>
        <label for="nombres">Nombres</label>
        <input type="text" name="nombres" id="nombres">
        <br>
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos" id="apellidos">
        <br>
        <label for="sexo">Sexo</label>
        <input type="radio" name="sexo" id="F" value="F">M
        <input type="radio" name="sexo" id="M" value="M">F
        <br>
        <label for="direccion">Dirección</label>
        <input type="text" name="direccion" id="direccion">
        <br>
        <label for="correo">Correo</label>
        <input type="email" name="correo" id="correo">
        <br>
        <label for="celular">Celular</label>
        <input type="text" name="celular" id="celular">
        <br>
        <label for="ciudad">Ciudad</label>
        <select name="ciudad_id" id="ciudad">
            @foreach ($ciudades as $ciudad)
            <option value="{{ $ciudad->id }}">{{ $ciudad->nombre }}</option>
            @endforeach
        </select>
        <br>
        <button type="submit">Guardar</button>
    </form>
@endsection