<?php

use App\Http\Controllers\CiudadController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ParticipanteController;


Route::resource('participantes', ParticipanteController::class);
Route::resource('ciudades', CiudadController::class)->parameters([
    'ciudades' => 'ciudad' // Aquí cambiamos 'ciudades' por 'ciudad'
]);
Route::view('/', 'layout.main')->name('home');
