<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    use HasFactory;
    protected $fillable=['carnet_identidad','nombres','apellidos','sexo','direccion','correo','celular','ciudad_id'];
    public function ciudad()
    {
        //relacion con el modelo ciudad          estandar
        return $this->belongsTo(Ciudad::class, 'ciudad_id','id');
    }
}
