<?php

namespace App\Http\Controllers;

use App\Models\Participante;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ciudad;
use GuzzleHttp\Promise\Create;

class ParticipanteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $participantes = Participante::with('ciudad')->get();
        return view('participantes.index',['participantes'=>$participantes]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $ciudades = Ciudad::all();
        return view('E.create', ['ciudades' => $ciudades]);
    }
    
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //depurar y ver valores
        // dd($request->all());
        Participante::create($request->all());
        return redirect()->route('participantes.index');

  
    }

    /**
     * Display the specified resource.
     */
    public function show(Participante $participante)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Participante $participante)
    {
        // Muestra los valores del objeto
        // dd($participante);
        $ciudades = Ciudad::all();
        return view('participantes.edit', ['participante' => $participante], ['ciudades' => $ciudades]);
    }
    

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Participante $participante)
    {
        $participante->update($request->all());
        return redirect()->route('participantes.index')->with('success', 'Participante actualizado correctamente.');
    }
    

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Participante $participante)
    {
   // Elimina el participante de la base de datos
   $participante->delete();
    
   // Redirecciona a la página index con un mensaje de éxito
   return redirect()->route('participantes.index')->with('success', 'Participante eliminado correctamente.');    }
}
