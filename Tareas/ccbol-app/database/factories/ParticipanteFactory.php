<?php

namespace Database\Factories;

use App\Models\Ciudad;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Participante>
 */
class ParticipanteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "carnet_identidad" => fake()->unique()->numberBetween(10000000, 99999999),
            "nombres" => fake()->firstName(),
            "apellidos" => fake()->lastName(),
            "sexo" => fake()->randomElement(['M', 'F']), 
            "direccion" => fake()->address(),
            "correo" => fake()->unique()->safeEmail(),
            "celular" => fake()->phoneNumber(),
            "ciudad_id" => Ciudad::all()->random()->id
            
        ];
    }
}
