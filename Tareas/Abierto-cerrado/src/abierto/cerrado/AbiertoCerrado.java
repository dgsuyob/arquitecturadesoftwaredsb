/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package abierto.cerrado;

/**
 *
 * @author david
 */
public class AbiertoCerrado {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Coche[] arrayCoches = {
            new CocheAudi(), new CocheChevrolet()
        };
        
        for(Coche coche : arrayCoches){
            System.out.println(coche.precioMedioCoche());
        }
    }

}
