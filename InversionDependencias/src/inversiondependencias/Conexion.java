/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package inversiondependencias;

/**
 *
 * @author david
 */
public interface Conexion {
    Dato getDatos();
    void setDatos();
}
