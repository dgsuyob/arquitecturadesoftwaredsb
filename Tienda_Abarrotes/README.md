# Tienda_Abarrotes

## Descripción del Proyecto
Este proyecto es una tienda de abarrotes en línea desarrollada en Node.js.

## Inicialización del Proyecto
Para inicializar el proyecto, sigue estos pasos:

1. Clona este repositorio en tu máquina local.
2. Abre una terminal y navega hasta el directorio raíz del proyecto.
3. Ejecuta el siguiente comando para inicializar el proyecto:

    ```bash
    npm init -y
    ```

4. Luego, instala las dependencias necesarias ejecutando el siguiente comando:

    ```bash
    npm install express ejs express-myconnection morgan mysql nodemon puppeteer crypto express-session multer
    ```

## Uso
Para iniciar el servidor de desarrollo, ejecuta el siguiente comando en la terminal:

```bash
nodemon app
