const connection = require("express-myconnection");
const productosModel = require('../models/Sprint_dos_eddy/productoCRUD')
const categoriasModel = require('../models/categoriaCRUD')

const controller = {}

controller.list = (req, res) => {
    if(req.session.loggedin){
        req.getConnection((err, conn) => {
            productosModel.getAllProductos(conn, (err, productos) => {
                if (err) {
                    res.json(err);
                }
                categoriasModel.getAllCategorias(conn, (err, categorias) => { // Obtén las categorías
                    if (err) {
                        res.json(err);
                    }
                    res.render('stock', { login: true, rol: req.session.rol, ver: productos, categoria: categorias, currentUrl: '/stock' });
                });
            });
        });
    }else{
        res.redirect('/login');
    }
    
}

controller.edit = (req, res) => {
    if(req.session.loggedin){
        const { id } = req.params;
        req.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                res.redirect(500, '/stock');
            } else {
                conn.query('SELECT * FROM producto WHERE id = ?', [id], (err, producto) => {
                    if (err) {
                        console.log(err);
                        res.redirect(500, '/stock');
                    } else {
                        res.render('stock_edit', {
                            data: producto[0],
                            login: true, 
                            rol: req.session.rol,
                            currentUrl: '/stock'
                        });
                    }
                });
            }
        });
    }else{
        res.redirect('/login');
    }
    
};


controller.update = (req, res) => {
    const { id } = req.params;
    let { stock } = req.body;
    console.log(id, stock);
    req.getConnection((err, conn) => {
        if (err) {
            console.log(err);
            return res.status(500).send("Error de servidor");
        }

        // Obtener el stock actual del producto
        conn.query('SELECT stock FROM producto WHERE id = ?', [id], (err, rows) => {
            if (err) {
                console.log(err);
                return res.status(500).send("Error de servidor");
            }

            if (rows.length !== 1) {
                return res.status(404).send("Producto no encontrado");
            }

            const currentStock = rows[0].stock;

            // Sumar el stock actual con el nuevo stock
            const updatedStock = currentStock + parseInt(stock);

            // Actualizar el stock en la base de datos
            conn.query('UPDATE producto SET stock = ? WHERE id = ?', [updatedStock, id], (err, result) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send("Error de servidor");
                }
                
                res.redirect('/stock');
            });
        });
    });
};


module.exports = controller;
