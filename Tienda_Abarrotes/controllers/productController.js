const Product = require('../models/product');
const connection = require("express-myconnection");

const products = new Map();

function addProduct(name, initialStock) {
    const product = new Product(name, initialStock);
    products.set(name, product);
    return product;
}

function getproduct() {
    return products;
}

function sellProduct(req, name, amount) {
    req.getConnection( async (err, conn) => {
        if (err) {
            res.json(err);
        }
         const product = products.get(name);
            if (!product) {
                throw new Error('Producto no encontrado');
            }
            const stock = product.reduceStock(amount)
            //console.log(stock);
            await conn.query(`UPDATE producto SET stock = ? WHERE nombre = ?`, [parseInt(stock), `${name}`]);
        
    })

}



module.exports = {
    addProduct,
    sellProduct,
    getproduct
};
