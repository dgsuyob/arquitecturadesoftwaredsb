const modelCategoria = require('../models/categoriaCRUD');

const controller = {};

controller.list = (req, res) => {
    if(req.session.loggedin){
        req.getConnection((err, conn) => {
            modelCategoria.getAllCategorias(conn, (err, categorias) => {
                if (err) {
                    res.json(err);
                }
                console.log(categorias);
                res.render('categoria', {
                    data: categorias, 
                    login: true, 
                    rol: req.session.rol,
                    currentUrl: '/productos'
                });
            });
        });
    }else{
        res.redirect('/login');
    }
    
};


controller.save = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        modelCategoria.createCategoria(conn, data, (err, categoria) => {
            res.redirect('/categoria');
        });
    });
};

controller.delete = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        modelCategoria.deleteCategoria(conn, id, (err, rows) => {
            res.redirect('/categoria');
        });
    });
};

controller.edit = (req, res) => {
    if(req.session.loggedin){
        const { id } = req.params;
        req.getConnection((err, conn) => {
            modelCategoria.getCategoriaById(conn, id, (err, categoria) => {
                res.render('categoria_edit', {
                    data: categoria[0],
                    login: true, 
                    rol: req.session.rol,
                    currentUrl: '/productos'
                });
            });
        });
    }else{
        res.redirect('/login');
    }
    
};

controller.update = (req, res) => {
    const { id } = req.params;
    const newCategoria = req.body;
    req.getConnection((err, conn) => {
        modelCategoria.updateCategoria(conn, id, newCategoria, (err, rows) => {
            res.redirect('/categoria');
        });
    });
};

controller.serach = (req, res) => {
    
};

module.exports = controller;