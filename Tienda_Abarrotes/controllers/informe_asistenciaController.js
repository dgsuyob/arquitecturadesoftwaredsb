const modelCategoria = require('../models/informe_asistenciaModel');

const controller = {};


controller.principal=(req, res)=>{
    res.render('informe_asistencia')
}

controller.generarListaAsistencia = (req, res) => {
    const  {fecha}  = req.query;
   console.log(fecha)

   req.getConnection((err, conn) => {
        if (err) {
            console.error('Error de conexión a la base de datos', err);
            res.status(500).send('Error de conexión a la base de datos');
            return;
        }

        const query = `
            SELECT 
                empleado.nombre, 
                empleado.apellido_paterno, 
                empleado.apellido_materno, 
                TIME(registro_hora.hora_entrada) as hora_entrada, 
                TIME(registro_hora.hora_salida) as hora_salida 
            FROM 
                empleado 
            JOIN 
                registro_hora ON empleado.id = registro_hora.id_empleado 
            WHERE 
                DATE(registro_hora.hora_entrada) =  ?
        `;

        conn.query(query, [fecha], (err, result) => {
            if (err) {
                console.error('Error al obtener la lista de asistencia', err);
                res.status(500).send('Error al obtener la lista de asistencia');
                return;
            }

            res.render('informe', { data: result, fecha: fecha });
            console.log(result)
       
        });
    });
};



module.exports = controller;