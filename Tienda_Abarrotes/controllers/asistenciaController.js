const modelCategoria = require('../models/asistenciaModel');

const controller = {};

controller.principal=(req, res)=>{
    const empleado = [];
    if(req.session.loggedin){
        res.render('asistencia', {login: true, rol: req.session.rol, empleado: empleado, currentUrl: '/asistencia' });
    }else{
        res.redirect('/login');
    }
    
}


controller.registrarAsistencia = (req, res) => {
    if(req.session.loggedin){
        const { numero_contrato } = req.body; // Cambio a req.body para obtener el código de empleado
        const now = new Date();
        const options = {
            timeZone: 'America/La_Paz', // Configura la zona horaria para Bolivia
            hour12: false // Utiliza el formato de 24 horas
        };
        const fecha = now.toISOString().split('T')[0]; // Obtiene la fecha en formato YYYY-MM-DD
        const hora = now.toLocaleTimeString('es-BO', options); // Obtiene la hora en formato HH:MM:SS
        const fechaHora = `${fecha} ${hora}`;
        const a=`'${fecha}%'`
        console.log(numero_contrato); // Imprime el código de empleado
        console.log(fecha);
        console.log(hora);
        console.log(a)
        //res.send('Registro de entrada realizado correctamente');
        req.getConnection((err, conn) => {
            if (err) {
                console.error('Error de conexión a la base de datos', err);
                res.status(500).send('Error de conexión a la base de datos');
                return;
            }
    
            // Busca si ya existe un registro para el empleado en la fecha actual
            conn.query('SELECT COUNT(*) AS registros FROM registro_hora WHERE id_empleado = (SELECT id FROM empleado WHERE numero_contrato = ?) AND  DATE(hora_entrada) = CURDATE();', [numero_contrato], (err, result) => {
                if (err) {
                    console.error('Error al buscar el registro de asistencia', err);
                    res.status(500).send('Error al buscar el registro de asistencia');
                    return;
                }
                //console.log(result)
                const registros = result[0].registros;
                console.log(registros)
               //res.send('Registro de entrada realizado correctamente');
               if (registros >0) {
                //console.log("1312312")
                // Ya existe un registro para el empleado en la fecha actual, actualiza la hora de salida
                conn.query('UPDATE registro_hora SET hora_salida = NOW() WHERE id_empleado = (SELECT id FROM empleado WHERE numero_contrato = ?) AND DATE(hora_entrada) =?', [ numero_contrato,fecha], (err, result) => {
                    if (err) {
                        console.error('Error al actualizar el registro de asistencia', err);
                        res.status(500).send('Error al actualizar el registro de asistencia');
                        return;
                    }
                    conn.query('SELECT e.nombre, e.apellido_paterno, e.apellido_materno FROM empleado e INNER JOIN registro_hora rh ON e.id = rh.id_empleado WHERE e.numero_contrato = ? AND DATE(rh.hora_entrada) = CURDATE()', [numero_contrato], (err, result) => {
                        if (err) {
                            console.error('Error al buscar el registro de asistencia', err);
                            res.status(500).send('Error al buscar el registro de asistencia');
                            return;
                        }
            
                        const empleadoEncontrado = result[0];
                        //console.log(empleadoEncontrado);
                        const tipoRegistro = 'Salida';
                        res.render('asistencia', { login: true, rol: req.session.rol, empleado: [empleadoEncontrado], tipoRegistro, currentUrl: '/asistencia' });
                    });
                    //res.render('asistencia');
                    
                });
            }else {
                console.log(numero_contrato)
                // No existe un registro para el empleado en la fecha actual, crea un nuevo registro con la hora de entrada
                conn.query('INSERT INTO registro_hora (id_empleado, hora_entrada) VALUES ((SELECT id FROM empleado WHERE numero_contrato = ?), NOW())', [numero_contrato], (err, result) => {
                    if (err) {
                        console.error('Error al registrar la hora de entrada', err);
                        res.status(500).send('Error al registrar la hora de entrada');
                        return;
                    }
                    conn.query('SELECT e.nombre, e.apellido_paterno, e.apellido_materno FROM empleado e INNER JOIN registro_hora rh ON e.id = rh.id_empleado WHERE e.numero_contrato = ? AND DATE(rh.hora_entrada) = CURDATE()', [numero_contrato], (err, result) => {
                        if (err) {
                            console.error('Error al buscar el registro de asistencia', err);
                            res.status(500).send('Error al buscar el registro de asistencia');
                            return;
                        }
            
                        const empleadoEncontrado = result[0];
                        //console.log(empleadoEncontrado);
                        const tipoRegistro = 'Entrada';
                        res.render('asistencia', { login: true, rol: req.session.rol, empleado: [empleadoEncontrado], tipoRegistro, currentUrl: '/asistencia' });
                    });
                    //res.render('asistencia');
                });
            }
            
            });
        });
    }else{
        res.redirect('/login');
    }
    
};


module.exports = controller;