const ListController = require('./listaController');

class transferenciaController {
    generarTransferencia(req, res) {
        if (req.session.loggedin) {
            const productList = new ListController();
            const products = productList.getProductList();
            console.log(products)
            res.render('transferencia', { login: true, rol: req.session.rol, products: products });
        } else {
            res.redirect('/login');
        }

    }

    generarFactura(req, res) {
        if (req.session.loggedin) {
            const products = productList.getProductList();
            const totalPrice = products.reduce((total, product) => total + parseFloat(product.precio), 0).toFixed(2);
            res.render('factura', {login: true, rol: req.session.rol, products: products, totalPrice: totalPrice });
        } else {
            res.redirect('/login');
        }
    }
}

module.exports = new transferenciaController();
