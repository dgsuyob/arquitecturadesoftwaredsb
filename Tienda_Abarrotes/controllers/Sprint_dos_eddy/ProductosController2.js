const connection = require("express-myconnection");
const productosModel = require('../../models/Sprint_dos_eddy/productoCRUD')
const categoriasModel = require('../../models/categoriaCRUD') // Asegúrate de tener este modelo

const productos = {}

productos.list = (req, res) => {
    if(req.session.loggedin){
        req.getConnection((err, conn) => {
            productosModel.getAllProductos(conn, (err, productos) => {
                if (err) {
                    res.json(err);
                }
                categoriasModel.getAllCategorias(conn, (err, categorias) => { // Obtén las categorías
                    if (err) {
                        res.json(err);
                    }
                    res.render('Sprint_dos_eddy/crud_productos', {login: true, rol: req.session.rol, ver: productos, categoria: categorias, currentUrl: '/Lista_productos' });
                });
            });
        });
    }else{
        res.redirect('/login');
    }
    
}

module.exports = productos;
