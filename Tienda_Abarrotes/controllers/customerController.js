const modelCustomers = require('../models/customerCRUD');

const controller = {};

controller.list = (req, res) => {
    if (req.session.loggedin) {
        req.getConnection((err, conn) => {
            modelCustomers.getAllCustomers(conn, (err, customers) => {
                if (err) {
                    res.json(err);
                }
                console.log(customers);
                res.render('customers', {
                    data: customers,
                    login: true,
                    rol: req.session.rol,
                    currentUrl: '/productos'
                });
            });
        });
    } else {
        res.redirect('/login');
    }

};

controller.save = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        modelCustomers.createCustomer(conn, data, (err, customer) => {
            res.redirect('/customer');
        });
    });
};

controller.delete = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        modelCustomers.deleteCustomer(conn, id, (err, rows) => {
            res.redirect('/customer');
        });
    });
};

controller.edit = (req, res) => {
    if (req.session.loggedin) {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            modelCustomers.getCustomerById(conn, id, (err, customer) => {
                res.render('customer_edit', {
                    data: customer[0],
                    login: true,
                    rol: req.session.rol,
                    currentUrl: '/productos'
                });
            });
        });
    } else {
        res.redirect('/login');
    }

};

controller.update = (req, res) => {
    const { id } = req.params;
    const newCustomer = req.body;
    req.getConnection((err, conn) => {
        modelCustomers.updateCustomer(conn, id, newCustomer, (err, rows) => {
            res.redirect('/customer');
        });
    });
};

controller.serach = (req, res) => {
    // Implementar la búsqueda
};

module.exports = controller;
