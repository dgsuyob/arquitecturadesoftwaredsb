const modelempleado = require('../models/empleadoCRUD');
const controller = {};

controller.list = (req, res) => {
    if (req.session.loggedin) {
        req.getConnection((err, conn) => {
            modelempleado.getAllEmpleados(conn, (err, empleados) => {
                if (err) {
                    res.json(err);
                }
                console.log(empleados);
                res.render('registro_empleado', {
                    data: empleados,
                    login: true,
                    rol: req.session.rol,
                    currentUrl: '/empleado'
                });
            });
        });
    } else {
        res.redirect('/login');
    }
};


controller.save = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        modelempleado.createEmpleados(conn, data, (err, empleado) => {
            res.redirect('/empleado');
        });
    });
};

controller.delete = (req, res) => {
    const { id } = req.params;
    console.log(id);
    req.getConnection((err, conn) => {
        modelempleado.deleteEmpleados(conn, id, (err, rows) => {
            res.redirect('/empleado');
        });
    });
};

controller.edit = (req, res) => {
    if (req.session.loggedin) {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            modelempleado.getEmpleadosById(conn, id, (err, empleados) => {
                res.render('empleado_edit', {
                    data: empleados[0],
                    login: true, 
                    rol: req.session.rol,
                    currentUrl: '/empleado'
                });
            });
        });
    } else {
        res.redirect('/login');
    }
};

controller.update = (req, res) => {
    const { id } = req.params;
    const newEmpleado = req.body;
    req.getConnection((err, conn) => {
        modelempleado.updateEmpleados(conn, id, newEmpleado, (err, rows) => {
            res.redirect('/empleado');
        });
    });
};
module.exports = controller;