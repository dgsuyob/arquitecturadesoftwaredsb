// controllers/productListController.js

const lista_model = require('../models/lista');

class listaController {
    constructor() {
        this.model = new lista_model();
    }

    addProduct(product) {
        this.model.addProduct(product);
    }

    getProductList() {
        return this.model.getProducts();
    }

    clearProductList() {
        this.model.clearProducts();
    }

    deleteProduct(productIndex) {
        this.model.deleteProduct(productIndex);
    }
}

module.exports = listaController;
