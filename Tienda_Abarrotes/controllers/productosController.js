const connection = require("express-myconnection");

const controller = {}

controller.list = (req, res) => {
    if(req.session.loggedin){
        req.getConnection((err, conn) => {
            conn.query('SELECT producto.*, fabricante.nombre AS nombre_fabricante, categoria.nombre AS nombre_categoria FROM producto JOIN fabricante ON producto.fabricante_id = fabricante.id JOIN categoria ON producto.categoria_id = categoria.id', (err, productos) => {
                if (err) {
                    res.json(err);
                }
                conn.query('SELECT * FROM fabricante', (err, fabricantes) => {
                    if (err) {
                        res.json(err);
                    }
                    conn.query('SELECT * FROM categoria', (err, categorias) => {
                        if (err) {
                            res.json(err);
                        }
                        res.render('producto', {
                            login: true, 
                            rol: req.session.rol,
                            data: productos,
                            fabricantes: fabricantes,
                            categorias: categorias,
                            currentUrl: '/productos'
                        });
                    });
                });
            });
        });
    }else{
        res.redirect('/login');
    }
    
}

controller.save = (req,res)=>{
    const data = req.body;
    req.getConnection((err,conn) => {
        conn.query('INSERT INTO producto set ?',[data],(err, producto) => {
            res.redirect('/productos');
        });
    })
};

controller.edit = (req, res) => {
    if(req.session.loggedin){
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM producto WHERE id = ?', [id], (err, producto) => {
                if (err) {
                    console.log(err);
                    res.redirect('/productos');
                } else {
                    conn.query('SELECT * FROM fabricante', (err, fabricantes) => {
                        if (err) {
                            console.log(err);
                            res.redirect('/productos');
                        } else {
                            conn.query('SELECT * FROM categoria', (err, categorias) => {
                                if (err) {
                                    console.log(err);
                                    res.redirect('/productos', {currentUrl: '/productos'});
                                } else {
                                    res.render('producto_edit', {
                                        data: producto[0],
                                        login: true, 
                                        rol: req.session.rol,
                                        fabricantes: fabricantes,
                                        categorias: categorias,
                                        currentUrl: '/productos'
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    }else{
        res.redirect('/login');
    }
    
};

controller.update = (req, res) => {
    const { id } = req.params;
    const newProducto = req.body;
    req.getConnection((err, conn) => {
        conn.query('UPDATE producto SET ? WHERE id = ?', [newProducto, id], (err, rows) => {
            if (err) {
                console.log(err);
            }
            res.redirect('/productos', {currentUrl: '/productos'}); 
        });
    });
};

controller.delete = (req,res) =>{
    const {id}= req.params;
    req.getConnection((err,conn) =>{
        conn.query('DELETE FROM producto WHERE id = ?',[id],(err,rows)=>{
            res.redirect('/productos', {currentUrl: '/productos'});
        });  
    });
};

controller.searchByName = (req, res) => {
    if(req.session.loggedin){
        const { nombre } = req.query;
        req.getConnection((err, conn) => {
            conn.query('SELECT producto.*, fabricante.nombre AS nombre_fabricante, categoria.nombre AS nombre_categoria FROM producto JOIN fabricante ON producto.fabricante_id = fabricante.id JOIN categoria ON producto.categoria_id = categoria.id WHERE producto.nombre LIKE ?', [`%${nombre}%`], (err, productos) => {
                if (err) {
                    res.json(err);
                } else {
                    res.render('producto', {
                        data: productos,
                        login: true, 
                        rol: req.session.rol,
                        fabricantes: [],
                        categorias: [],
                        currentUrl: '/productos'
                    });
                }
            });
        });
    }else{
        res.redirect('/login');
    }
};


module.exports = controller;
