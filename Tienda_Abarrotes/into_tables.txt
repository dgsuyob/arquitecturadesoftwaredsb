INSERT INTO `categoria` VALUES (1,'Carnes','Productos cárnicos'),
(2,'Frutas y Verduras','Productos frescos'),
(3,'Lácteos','Productos lácteos'),
(4,'Abarrotes Secos','Productos no perecederos'),
(5,'Bebidas','Bebidas y refrescos'),
(6,'Panadería','Productos de panadería');

INSERT INTO `empleado` VALUES (1,'Carlos','Gutierrez','Diaz',1001,'2023-05-15','9:00 - 17:00',1),
(2,'Ana','Hernandez','Lopez',1002,'2024-01-10','8:00 - 16:00',2),
(3,'Luis','Rodriguez','Sanchez',1003,'2023-11-20','10:00 - 18:00',3);

INSERT INTO `fabricante` VALUES (1,'IMPORTADORA LUCIA','123-456-789'),
(2,'IMPORTADORA MIRANDA','987-654-321'),
(3,'IMPORTADORA AMANECER','456-789-123');

INSERT INTO `producto` VALUES (1,1,1,'Carne de res',1001,8.99,10,'2023-01-10',NULL,'Filete de res'),
(2,2,2,'Manzanas',4506,1.99,60,'2023-02-05',NULL,'Manzanas rojas'),
(3,3,3,'Leche',2001,3.49,80,'2023-01-15','2023-03-15','Leche entera'),
(4,1,4,'Arroz',3001,2.99,250,'2023-01-20','2024-01-20','Arroz blanco'),
(5,2,5,'Refresco de cola',78,0.99,200,'2023-02-01','2024-02-01','Refresco de cola 2L'),
(6,3,6,'Pan blanco',478,1.49,10,'2023-02-10','2023-02-12','Pan blanco de trigo');

INSERT INTO `metodo_pago` VALUES (1,'Efectivo'),
(2,'Transferencia bancaria');