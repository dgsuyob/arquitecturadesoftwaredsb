const express = require("express");
const myConnection = require("express-myconnection");
const morgan = require("morgan");
const mysql = require("mysql");
const path = require("path");
const app = express();
const PORT = 2000;

const session = require("express-session");
const puppeteer = require("puppeteer"); // Importa el router para la ruta de categorías
const multer = require("multer");


app.set("port", process.env.PORT || 2000);
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(session({
  secret: 'secret', resave: true, saveUninitialized: true
}));

app.use(express.static(path.join(__dirname, "public")));

app.use(morgan("dev"));
app.use(
  myConnection(
    mysql,
    {
      host: "localhost",
      user: "root",
      password: "",
      port: 3306,
      database: "bd_tienda_abarrotes",
    },
    "single"
  )
);


app.set("view engine", "ejs");
// Ruta para obtener las categorías
app.use("/lista_productos", require("./routes/Sprint_dos_eddy/crud_productos"));
app.use("/asistencia", require("./routes/asistencia"));
app.use("/ventas", require("./routes/ventas"));
app.use("/categoria", require("./routes/categoria"));
app.use("/productos", require("./routes/productos"));
app.use("/stock", require("./routes/stock"));
app.use("/customer", require("./routes/customer"));
app.use("/empleado", require("./routes/empleado"));
app.use("/home", require("./routes/principal"));
app.use("/login", require("./routes/login"));
app.use("/logout", require("./routes/logout"));

app.post("/generar-pdf", async (req, res) => {
  const html = req.body.html;
  const ocultarHTML = req.body.ocultar;
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // Añade el HTML del div ocultar al final del HTML del div mostrar
  const htmlCompleto = html + ocultarHTML;

  // Inyecta el CSS en la página
  await page.setContent(htmlCompleto, { waitUntil: "networkidle2" });
  await page.addStyleTag({ path: "public/styles_ventas_dia.css" }); // Ajusta la ruta según sea necesario

  await page.evaluate(() => {
    const ocultarDiv = document.querySelector("#ocultar");
    ocultarDiv.style.display = "none";
  });

  const pdfBuffer = await page.pdf();

  res.setHeader("Content-Type", "application/pdf");
  res.setHeader(
    "Content-Disposition",
    "attachment; filename=ventas_diarias.pdf"
  );
  res.send(pdfBuffer);

  await browser.close();
});

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/imgProductos');
  },
  filename: function (req, file, cb) {
    const date = new Date().toISOString().replace(/:/g, '-');
    cb(null, date + file.originalname);
  },
});

const upload = multer({ storage: storage });

// Iniciar servidor
app.listen(PORT, () => {
  console.log(`Servidor corriendo en el puerto http://localhost:${PORT}/login`);
});
