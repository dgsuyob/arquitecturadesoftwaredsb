// indexRoutes.js
const express = require('express');
const path = require('path');
const router = express.Router();

router.use(express.static(path.join(__dirname, 'public')));

router.get('/', (req, res) => {
    if (req.session.loggedin) {
        res.render('view_principal', {
            login: true,
            rol: req.session.rol,
            currentUrl: '/home'
        });
    } else {
        res.redirect('/login');
    }
});


module.exports = router;
