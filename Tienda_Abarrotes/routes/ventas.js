const express = require("express");
const productController = require("../controllers/productController");
const ListController = require("../controllers/listaController");
const Observer = require("../Observer");

const productList = new ListController();
const observer = new Observer();
const router = express.Router();

// READ

router.get('/', (req, res) => {
    if (req.session.loggedin) {
        req.getConnection((err, connection) => {
            if (err) {
                throw err;
            } else {
                connection.query('SELECT * FROM producto', (error, results) => {
                    if (error) {
                        throw error;
                    } else {
                        results.forEach(result => {
                            const newProduct = productController.addProduct(result.nombre, result.stock);
                            newProduct.addObserver(observer);
                        });
                        const products = productList.getProductList();
                        let totalPrice = products.reduce((total, product) => total + parseFloat(product.precio), 0).toFixed(2);
                        res.render('read', { login: true, rol: req.session.rol, results: results, products: products, totalPrice: totalPrice, currentUrl: '/ventas' });
                    }
                });
            }
        });
    } else {
        res.redirect('/login');
    }
});

// LISTA_PRODUCTO
router.post('/', (req, res) => {
    const { productName, amount } = req.body;
    req.getConnection((err, connection) => {
        if (err) {
            throw err;
        } else {
            let query;
            let queryParams;
            // Verificar si productName es un código o un nombre
            const codeRegex = /^[a-zA-Z]+$/; // Expresión regular para letras
            if (codeRegex.test(productName)) { // Si es una cadena de letras, se considera un código
                query = 'SELECT precio FROM producto WHERE codigo = ?';
                queryParams = [productName];
            } else { // De lo contrario, se considera un nombre
                query = 'SELECT precio FROM producto WHERE nombre = ?';
                queryParams = [productName];
            }

            connection.query(query, queryParams, (error, results) => {
                if (error) {
                    throw error;
                } else {
                    if (results.length > 0) {
                        const product = {
                            nombre: productName, // Si es un código, este será el nombre temporal
                            cantidad: amount,
                            precio: Math.round(results[0].precio * amount * 100) / 100
                        };
                        // Si el productName es un código, buscar el nombre correspondiente
                        if (codeRegex.test(productName)) {
                            connection.query('SELECT nombre FROM producto WHERE codigo = ?', [productName], (err, nameResults) => {
                                if (err) {
                                    throw err;
                                } else {
                                    if (nameResults.length > 0) {
                                        product.nombre = nameResults[0].nombre;
                                    }
                                    productList.addProduct(product);
                                }
                            });
                        } else {
                            productList.addProduct(product);
                        }
                    }
                }
            });
        }
  });
});


router.post('/delete', (req, res) => {
    const productIndexToDelete = req.body.productIndexToDelete;
    //console.log(productIdToDelete);
    productList.deleteProduct(productIndexToDelete);
    res.redirect('/ventas');
});

// PAGOS

router.get('/efect', (req, res) => {
    if (req.session.loggedin) {
        const products = productList.getProductList();
        const totalPrice = products.reduce((total, product) => total + parseFloat(product.precio), 0).toFixed(2);
        res.render('factura_efect', { login: true, rol: req.session.rol, products: products, totalPrice: totalPrice, currentUrl: '/ventas' });
    } else {
        res.redirect('/login');
    }
});

router.post("/efect", async (req, res) => {
    try {
        const products = productList.getProductList();
        for (const product of products) {
            await productController.sellProduct(
                req,
                product.nombre,
                product.cantidad
            );

            req.getConnection((err, connection) => {
                if (err) {
                    throw err;
                } else {
                    // Obtener el id del producto de la base de datos
                    connection.query(
                        "SELECT id FROM producto WHERE nombre = ?",
                        [product.nombre],
                        (error, results) => {
                            if (error) throw error;

                            if (results.length > 0) {
                                const producto_id = results[0].id;

                                const query =
                                    "INSERT INTO ventas_diarias (producto_id, cantidad_vendida, total_ventas, fecha) VALUES (?, ?, ?, CURDATE()) ON DUPLICATE KEY UPDATE cantidad_vendida = cantidad_vendida + VALUES(cantidad_vendida), total_ventas = total_ventas + VALUES(total_ventas)";
                                connection.query(
                                    query,
                                    [
                                        producto_id,
                                        product.cantidad,
                                        product.precio * product.cantidad,
                                    ],
                                    (error, result) => {
                                        if (error) throw error;
                                    }
                                );
                            } else {
                                console.error(
                                    "Producto no encontrado en la base de datos:",
                                    product.nombre
                                );
                            }
                        }
                    );
                }
            });
        }
        productList.clearProductList();
        res.redirect("/ventas");
    } catch (error) {
        res
            .status(500)
            .send(
                "Error al generar la factura y descontar el stock: " + error.message
            );
    }
});

router.get('/pago_transferencia', (req, res) => {
    if (req.session.loggedin) {
        const products = productList.getProductList();
        const totalPrice = products.reduce((total, product) => total + parseFloat(product.precio), 0).toFixed(2);
        res.render('transferencia', {login: true, rol: req.session.rol, products: products, totalPrice: totalPrice, currentUrl: '/ventas' })
    } else {
        res.redirect('/login');
    }

});

router.post("/pago_transferencia", async (req, res) => {
    try {
        const products = productList.getProductList();
        for (const product of products) {
            await productController.sellProduct(
                req,
                product.nombre,
                product.cantidad
            );

            req.getConnection((err, connection) => {
                if (err) {
                    throw err;
                } else {
                    // Obtener el id del producto de la base de datos
                    connection.query(
                        "SELECT id FROM producto WHERE nombre = ?",
                        [product.nombre],
                        (error, results) => {
                            if (error) throw error;

                            if (results.length > 0) {
                                const producto_id = results[0].id;

                                const query =
                                    "INSERT INTO ventas_diarias (producto_id, cantidad_vendida, total_ventas, fecha) VALUES (?, ?, ?, CURDATE()) ON DUPLICATE KEY UPDATE cantidad_vendida = cantidad_vendida + VALUES(cantidad_vendida), total_ventas = total_ventas + VALUES(total_ventas)";
                                connection.query(
                                    query,
                                    [
                                        producto_id,
                                        product.cantidad,
                                        product.precio * product.cantidad,
                                    ],
                                    (error, result) => {
                                        if (error) throw error;
                                    }
                                );
                            } else {
                                console.error(
                                    "Producto no encontrado en la base de datos:",
                                    product.nombre
                                );
                            }
                        }
                    );
                }
            });
        }
        productList.clearProductList();
        res.redirect("/ventas");
    } catch (error) {
        res
            .status(500)
            .send(
                "Error al generar la factura y descontar el stock: " + error.message
            );
     }
});



router.get("/v_diarias", function (req, res) {
    if(req.session.loggedin){
        req.getConnection((err, connection) => {
            if (err) {
                throw err;
            } else {
                connection.query("SELECT * FROM ventas_diarias", (error, ventas) => {
                    if (error) {
                        throw error;
                    } else {
                        connection.query("SELECT * FROM producto", (error, products) => {
                            if (error) {
                                throw error;
                            } else {
                                res.render("diarias", {
                                    login: true, 
                                    rol: req.session.rol,
                                    ventas: ventas,
                                    products: products,
                                    currentUrl: '/ventas'
                                });
                            }
                        });
                    }
                });
            }
        });
    }else{
        res.redirect('/login');
    }
    
});

router.get("/limpiar", function (req, res) {
    req.getConnection((err, connection) => {
        if (err) {
            throw err;
        } else {
            connection.query("TRUNCATE TABLE ventas_diarias", (error, results) => {
                if (error) {
                    throw error;
                } else {
                    res.redirect("/ventas");
                }
            });
        }
    });
});

module.exports = router;

