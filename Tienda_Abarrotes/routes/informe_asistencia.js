const express=require('express')
const router=express.Router()
const informe_asistenciaController=require('../controllers/informe_asistenciaController')



router.get('/',informe_asistenciaController.principal )
router.get('/list',informe_asistenciaController.generarListaAsistencia)


module.exports=router;