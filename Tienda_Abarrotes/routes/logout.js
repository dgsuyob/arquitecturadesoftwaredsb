const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    // Destruir la sesión
    req.session.destroy((err) => {
        if (err) {
            console.log(err);
        } else {
            // Redirigir al usuario a la página de inicio de sesión
            res.redirect('/login');
        }
    });
});

module.exports = router;