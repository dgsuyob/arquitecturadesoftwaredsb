//categoria.js
const express = require("express");
const router = express.Router();
const categoriaController = require("../controllers/categoriaController");

router.get("/", categoriaController.list);
router.post("/add", categoriaController.save);
router.get("/delete/:id", categoriaController.delete);

router.get("/update/:id", categoriaController.edit);
router.post("/update/:id", categoriaController.update);

router.get("/", (req, res) => {
  connection.query("SELECT * FROM categoria", (err, rows) => {
    if (err) return res.status(500).send(err);
    res.json(rows);
  });
});

module.exports = router;
