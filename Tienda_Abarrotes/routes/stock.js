const express=require('express')
const router=express.Router()
const stocksController=require('../controllers/stockController')


router.get('/',stocksController.list )
router.post('/update/:id', stocksController.update)

module.exports = router;