const express = require('express');
const { connect } = require('net');
const path = require('path');
const router = express.Router();
const crypto = require('crypto');

router.use(express.static(path.join(__dirname,'public')));

router.get('/', (req, res) => {
    res.render('login/login');
});

router.post('/home', async (req, res) => {
    const user = req.body.user;
    const password = req.body.password;
    if(user && password){
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM users WHERE user = ?', [user], async (error, results) => {
                if (results.length === 0 || results[0].password !== sha1(password)) {
                    res.render('login/login', {
                        alert: true,
                        alertTitle: 'Inicio de sesion fallido...',
                        alertMessage: 'Usuario y/o contraseña incorrectos',
                        alertIcon: 'error',
                        showConfirmButton: false,
                        timer: 2500,
                        ruta: 'login'
                    } );  
                }else{
                    req.session.loggedin = true;
                    req.session.rol = results[0].rol;
                    res.render('login/login', {
                        alert: true,
                        alertTitle: 'Iniciando sesion...',
                        alertMessage: '',
                        alertIcon: 'success',
                        showConfirmButton: false,
                        timer: 1500,
                        ruta: 'home'
                    } );
                }
            });
        });

    }
});


function sha1(input) {
    return crypto.createHash('sha1').update(input).digest('hex');
}

module.exports = router;