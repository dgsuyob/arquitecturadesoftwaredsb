const categoriaModel = {};

categoriaModel.getAllCategorias = (conn, callback) => {
    conn.query('SELECT * FROM categoria', callback);
};

categoriaModel.createCategoria = (conn, data, callback) => {
    conn.query('INSERT INTO categoria SET ?', data, callback);
};

categoriaModel.deleteCategoria = (conn, id, callback) => {
    conn.query('DELETE FROM categoria WHERE id = ?', id, callback);
};

categoriaModel.getCategoriaById = (conn, id, callback) => {
    conn.query('SELECT * FROM categoria WHERE id = ?', id, callback);
};

categoriaModel.updateCategoria = (conn, id, newData, callback) => {
    conn.query('UPDATE categoria SET ? WHERE id = ?', [newData, id], callback);
};

module.exports = categoriaModel;
