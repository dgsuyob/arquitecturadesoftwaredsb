const Pagoefectivo = {
    calculateTotal: function (productos) {
        return productos.reduce((acc, producto) => acc + producto.precio, 0);
    }
};

module.exports = Pagoefectivo;
