const customerModel = {};

customerModel.getAllCustomers = (conn, callback) => {
    conn.query('SELECT * FROM fabricante', callback);
};

customerModel.createCustomer = (conn, data, callback) => {
    conn.query('INSERT INTO fabricante SET ?', data, callback);
};

customerModel.deleteCustomer = (conn, id, callback) => {
    conn.query('DELETE FROM fabricante WHERE id = ?', id, callback);
};

customerModel.getCustomerById = (conn, id, callback) => {
    conn.query('SELECT * FROM fabricante WHERE id = ?', id, callback);
};

customerModel.updateCustomer = (conn, id, newData, callback) => {
    conn.query('UPDATE fabricante SET ? WHERE id = ?', [newData, id], callback);
};

module.exports = customerModel;
