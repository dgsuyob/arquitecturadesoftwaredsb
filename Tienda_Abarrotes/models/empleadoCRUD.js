
const empleadoModel = {};

empleadoModel.getAllEmpleados = (conn, callback) => {
    conn.query('SELECT id,nombre,apellido_paterno,apellido_materno,numero_contrato FROM empleado', callback);
};

empleadoModel.createEmpleados = (conn, data, callback) => {
    conn.query('INSERT INTO empleado SET ?', data, callback);
};

empleadoModel.deleteEmpleados = (conn, id, callback) => {
    const deleteEmpleadoQuery = 'DELETE FROM empleado WHERE id = ?';
    conn.query(deleteEmpleadoQuery, id, callback);
};

empleadoModel.getEmpleadosById = (conn, id, callback) => {
    conn.query('SELECT * FROM empleado WHERE id = ?', id, callback);
};

empleadoModel.updateEmpleados = (conn, id, newData, callback) => {
    conn.query('UPDATE empleado SET ? WHERE id = ?', [newData, id], callback);
};

module.exports = empleadoModel;