class lista {
    constructor() {
        this.products = [];
    }

    addProduct(product) {
        this.products.push(product);
    }

    getProducts() {
        return this.products;
    }

    deleteProduct(index) {
        // Elimina el producto en el índice especificado
        if (index >= 0 && index < this.products.length) {
            this.products.splice(index, 1);
        }
    }

    clearProducts() {
        this.products = [];
    }
}

module.exports = lista;
