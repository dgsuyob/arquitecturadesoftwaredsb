class product {

    constructor(name, initialStock) {
        this.name = name;
        this.stock = initialStock;
        this.observers = [];
    }

    addObserver(observer) {
        this.observers.push(observer);
    }

    reduceStock(amount) {
        this.stock -= amount;
        //console.log(this.stock);
        this.notifyObservers();

        return this.stock;
    }

    notifyObservers() {
        if (this.stock <= 10) {
            this.observers.forEach(observer => observer.notify(this));
        }
    }
}

module.exports = product;
