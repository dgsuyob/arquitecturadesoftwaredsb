/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author david
 */
public class Docente implements IDocente{
    private String nombre;
    private String ci;
    private String[] materias;

    public Docente(String nombre, String ci, String[] materias) {
        this.nombre = nombre;
        this.ci = ci;
        this.materias = materias;
    }

    @Override
    public String obtenerNombreCompleto() {
        return nombre;
    }

    @Override
    public String obtenerCI() {
        return ci;
    }

    @Override
    public String[] obtenerListaMaterias() {
        return materias;
    }

    @Override
    public void asignarNota(IEstudiante estudiante, String materia, double nota) {
        System.out.println("Se asignó la nota " + nota + " al estudiante " + estudiante.obtenerNombreCompleto() + " en la materia " + materia);
    }
}
