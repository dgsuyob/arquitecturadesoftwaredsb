
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */


/**
 *
 * @author david
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Seleccione una opcion:");
            System.out.println("1. Crear estudiante");
            System.out.println("2. Crear docente");
            System.out.println("3. Crear director");
            System.out.println("4. Salir");

            int opcion = scanner.nextInt();

            switch (opcion) {
                case 1 -> {
                    System.out.println("Ingrese el nombre del estudiante:");
                    String nombreEstudiante = scanner.next();
                    System.out.println("Ingrese la CI del estudiante:");
                    String ciEstudiante = scanner.next();
                    System.out.println("Ingrese el CU del estudiante:");
                    String cuEstudiante = scanner.next();
                    Estudiante estudiante = new Estudiante(nombreEstudiante, ciEstudiante, cuEstudiante);
                    System.out.println("Estudiante creado:");
                    System.out.println("Nombre: " + estudiante.obtenerNombreCompleto());
                    System.out.println("CI: " + estudiante.obtenerCI());
                    System.out.println("CU: " + estudiante.obtenerCU());
                }
                case 2 -> {
                    System.out.println("Ingrese el nombre del docente:");
                    String nombreDocente = scanner.next();
                    System.out.println("Ingrese la CI del docente:");
                    String ciDocente = scanner.next();
                    System.out.println("Ingrese las materias del docente (separadas por comas):");
                    scanner.nextLine(); // Consumir el salto de línea
                    String materiasDocenteInput = scanner.nextLine();
                    String[] materiasDocente = materiasDocenteInput.split(",");
                    Docente docente = new Docente(nombreDocente, ciDocente, materiasDocente);
                    System.out.println("Docente creado:");
                    System.out.println("Nombre: " + docente.obtenerNombreCompleto());
                    System.out.println("CI: " + docente.obtenerCI());
                    System.out.println("Materias:");
                    for (String materia : docente.obtenerListaMaterias()) {
                        System.out.println("- " + materia);
                    }
                }
                case 3 -> {
                    System.out.println("Ingrese el nombre del director:");
                    String nombreDirector = scanner.next();
                    System.out.println("Ingrese la CI del director:");
                    String ciDirector = scanner.next();
                    Director director = new Director(nombreDirector, ciDirector);
                    System.out.println("Director creado:");
                    System.out.println("Nombre: " + director.obtenerNombreCompleto());
                    System.out.println("CI: " + director.obtenerCI());
                }
                case 4 -> {
                    System.out.println("Saliendo del programa.");
                    return;
                }
                default -> System.out.println("Opción no válida.");
            }
        }
    }
}