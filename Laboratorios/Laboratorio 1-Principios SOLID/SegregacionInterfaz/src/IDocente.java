/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */

/**
 *
 * @author david
 */
public interface IDocente extends IPersona {
    String[] obtenerListaMaterias();
    void asignarNota(IEstudiante estudiante, String materia, double nota);
    
}
