/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author david
 */
public class Director implements IDirector {
    private String nombre;
    private String ci;

    public Director(String nombre, String ci) {
        this.nombre = nombre;
        this.ci = ci;
    }

    @Override
    public String obtenerNombreCompleto() {
        return nombre;
    }

    @Override
    public String obtenerCI() {
        return ci;
    }

    @Override
    public void gestionarPersonal(String accion, IPersona persona) {
        System.out.println("Se realizó la acción '" + accion + "' para el personal: " + persona.obtenerNombreCompleto());
    }
}
