/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author david
 */
public class Estudiante implements IEstudiante {
     private String nombre;
    private String ci;
    private String cu;
    private String[] cursosMatriculados;

    public Estudiante(String nombre, String ci, String cu) {
        this.nombre = nombre;
        this.ci = ci;
        this.cu = cu;
        this.cursosMatriculados = new String[0];
    }

    @Override
    public String obtenerNombreCompleto() {
        return nombre;
    }

    @Override
    public String obtenerCI() {
        return ci;
    }

    @Override
    public String obtenerCU() {
        return cu;
    }

    @Override
    public void matricularCurso(String curso) {
        String[] nuevosCursosMatriculados = new String[cursosMatriculados.length + 1];
        System.arraycopy(cursosMatriculados, 0, nuevosCursosMatriculados, 0, cursosMatriculados.length);
        nuevosCursosMatriculados[cursosMatriculados.length] = curso;
        cursosMatriculados = nuevosCursosMatriculados;
        System.out.println("Estudiante matriculado en el curso: " + curso);
    }
}