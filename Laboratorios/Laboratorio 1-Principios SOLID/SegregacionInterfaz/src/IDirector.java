/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */

/**
 *
 * @author david
 */
public interface IDirector extends IPersona{
    void gestionarPersonal(String accion, IPersona persona);
}
