/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inversiondependencias;

/**
 *
 * @author david
 */
class CorreoElectronico implements IDestinatario {
    @Override
    public void enviarNotificacion(String mensaje) {
        // Lógica para enviar notificación por correo electrónico
        System.out.println("Enviando correo electrónico: " + mensaje);
    }
}