/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inversiondependencias;

/**
 *
 * @author david
 */
class Notificador {
    private IDestinatario destinatario;

    public Notificador(IDestinatario destinatario) {
        this.destinatario = destinatario;
    }

    public void enviarNotificacion(String mensaje) {
        destinatario.enviarNotificacion(mensaje);
    }
}