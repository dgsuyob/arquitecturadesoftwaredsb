/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package inversiondependencias;

import java.util.Scanner;

/**
 *
 * @author david
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Seleccione el tipo de notificacion:");
            System.out.println("1. Correo electronico");
            System.out.println("2. SMS");
            System.out.println("3. Mensaje de texto");
            System.out.println("4. Salir");

            int opcion = scanner.nextInt();

            switch (opcion) {
                case 1 -> {
                    System.out.println("Ingrese el mensaje para enviar por correo electronico:");
                    scanner.nextLine();
                    String mensajeCorreo = scanner.nextLine();
                    Notificador notificadorCorreo = new Notificador(new CorreoElectronico());
                    notificadorCorreo.enviarNotificacion(mensajeCorreo);
                }
                case 2 -> {
                    System.out.println("Ingrese el mensaje para enviar por SMS:");
                    scanner.nextLine(); 
                    String mensajeSMS = scanner.nextLine();
                    Notificador notificadorSMS = new Notificador(new SMS());
                    notificadorSMS.enviarNotificacion(mensajeSMS);
                }
                case 3 -> {
                    System.out.println("Ingrese el mensaje para enviar por mensaje de texto:");
                    scanner.nextLine();
                    String mensajeTexto = scanner.nextLine();
                    Notificador notificadorTexto = new Notificador(new MensajeTexto());
                    notificadorTexto.enviarNotificacion(mensajeTexto);
                }
                case 4 -> {
                    System.out.println("Saliendo del programa.");
                    return;
                }
                default -> System.out.println("Opción no valida.");
            }
        }
    }
}