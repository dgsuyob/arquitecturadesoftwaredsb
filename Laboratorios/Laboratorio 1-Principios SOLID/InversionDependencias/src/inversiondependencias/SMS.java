/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inversiondependencias;

/**
 *
 * @author david
 */
class SMS implements IDestinatario {
    @Override
    public void enviarNotificacion(String mensaje) {
        // Lógica para enviar notificación por SMS
        System.out.println("Enviando SMS: " + mensaje);
    }
}