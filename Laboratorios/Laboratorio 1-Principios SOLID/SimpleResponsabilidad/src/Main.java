
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
/**
 *
 * @author david
 */
public class Main {

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            Calculadora calculadora = new Calculadora();

            while (true) {
                System.out.println("Seleccione la operacion:");
                System.out.println("1. Suma");
                System.out.println("2. Resta");
                System.out.println("3. Multiplicacion");
                System.out.println("4. Division");
                System.out.println("5. Salir");

                int opcion = scanner.nextInt();
                if (opcion == 5) {
                    System.out.println("Saliendo...");
                    break;
                }

                System.out.println("Ingrese el primer numero:");
                double num1 = scanner.nextDouble();
                System.out.println("Ingrese el segundo numero:");
                double num2 = scanner.nextDouble();

                double resultado = 0;

                switch (opcion) {
                    case 1 ->
                        resultado = calculadora.sumar(num1, num2);
                    case 2 ->
                        resultado = calculadora.restar(num1, num2);
                    case 3 ->
                        resultado = calculadora.multiplicar(num1, num2);
                    case 4 ->
                        resultado = calculadora.dividir(num1, num2);
                    default ->
                        System.out.println("Opcion no valida.");
                }

                System.out.println("El resultado es: " + resultado);
            }
        }
    }
}
