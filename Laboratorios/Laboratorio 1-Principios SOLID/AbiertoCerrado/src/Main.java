
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author david
 */
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                System.out.println("Seleccione el tipo de usuario:");
                System.out.println("1. Cliente");
                System.out.println("2. Proveedor");
                System.out.println("3. Administrador");
                System.out.println("4. Salir");
                
                int opcion = scanner.nextInt();
                if (opcion == 4) {
                    System.out.println("Saliendo...");
                    break;
                }
                
                Usuario usuario = null;
                switch (opcion) {
                    case 1 -> usuario = new Cliente();
                    case 2 -> usuario = new Proveedor();
                    case 3 -> usuario = new Administrador();
                    default -> System.out.println("Opción no válida.");
                }
                
                if (usuario != null) {
                    System.out.println("Ingrese nombre:");
                    usuario.setNombre(scanner.next());
                    System.out.println("Ingrese apellido:");
                    usuario.setApellido(scanner.next());
                    System.out.println("Ingrese sexo:");
                    usuario.setSexo(scanner.next());
                    System.out.println("Ingrese dirección:");
                    usuario.setDireccion(scanner.next());
                    System.out.println("Ingrese correo electrónico:");
                    usuario.setCorreoElectronico(scanner.next());
                    System.out.println("Ingrese celular:");
                    usuario.setCelular(scanner.next());
                    
                    System.out.println("Seleccione la operación:");
                    System.out.println("1. Crear");
                    System.out.println("2. Editar");
                    System.out.println("3. Eliminar");
                    int operacion = scanner.nextInt();
                    switch (operacion) {
                        case 1 -> usuario.crear();
                        case 2 -> {
                            System.out.println("Ingrese el ID del usuario a editar:");
                            usuario.setId(scanner.nextInt());
                            usuario.editar();
                        }
                        case 3 -> {
                            System.out.println("Ingrese el ID del usuario a eliminar:");
                            usuario.setId(scanner.nextInt());
                            usuario.eliminar();
                        }
                        default -> System.out.println("Operación no válida.");
                    }
                }
            }
        }
    }
}
