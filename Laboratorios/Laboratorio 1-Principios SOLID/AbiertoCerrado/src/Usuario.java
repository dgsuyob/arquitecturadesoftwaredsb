import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

class Usuario implements IUsuario {
    protected int id;
    protected String nombre;
    protected String apellido;
    protected String sexo;
    protected String direccion;
    protected String correoElectronico;
    protected String celular;

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }


    @Override
    public void crear() {
        try (Connection conexion = Conexion.conectar()) {
            String consulta = "INSERT INTO Usuario (nombre, apellido, sexo, direccion, correoElectronico, celular) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = conexion.prepareStatement(consulta, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, nombre);
            statement.setString(2, apellido);
            statement.setString(3, sexo);
            statement.setString(4, direccion);
            statement.setString(5, correoElectronico);
            statement.setString(6, celular);

            int filasInsertadas = statement.executeUpdate();

            if (filasInsertadas > 0) {
                // Obtener el ID generado para el nuevo usuario
                var generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt(1);
                }
                System.out.println("Usuario creado exitosamente.");
            }
        } catch (SQLException e) {
            System.out.println("Error al crear usuario: " + e.getMessage());
        }
    }

    @Override
    public void editar() {
        try (Connection conexion = Conexion.conectar()) {
            String consulta = "UPDATE Usuario SET nombre=?, apellido=?, sexo=?, direccion=?, correoElectronico=?, celular=? WHERE id=?";
            PreparedStatement statement = conexion.prepareStatement(consulta);

            statement.setString(1, nombre);
            statement.setString(2, apellido);
            statement.setString(3, sexo);
            statement.setString(4, direccion);
            statement.setString(5, correoElectronico);
            statement.setString(6, celular);
            statement.setInt(7, id);

            int filasActualizadas = statement.executeUpdate();

            if (filasActualizadas > 0) {
                System.out.println("Usuario actualizado exitosamente.");
            } else {
                System.out.println("No se encontró ningún usuario para actualizar.");
            }
        } catch (SQLException e) {
            System.out.println("Error al editar usuario: " + e.getMessage());
        }
    }

    @Override
    public void eliminar() {
        try (Connection conexion = Conexion.conectar()) {
            String consulta = "DELETE FROM Usuario WHERE id=?";
            PreparedStatement statement = conexion.prepareStatement(consulta);

            statement.setInt(1, id);

            int filasEliminadas = statement.executeUpdate();

            if (filasEliminadas > 0) {
                System.out.println("Usuario eliminado exitosamente.");
            } else {
                System.out.println("No se encontró ningún usuario para eliminar.");
            }
        } catch (SQLException e) {
            System.out.println("Error al eliminar usuario: " + e.getMessage());
        }
    }
}
