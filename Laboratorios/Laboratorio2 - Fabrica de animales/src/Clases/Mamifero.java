/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clases;

/**
 *
 * @author david
 */
public class Mamifero{
    private String nombre;
    private double temperatura;
    private int numeroDePatas;
    private String color;

    public Mamifero(String nombre, double temperatura, int numeroDePatas, String color) {
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.numeroDePatas = numeroDePatas;
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public int getNumeroDePatas() {
        return numeroDePatas;
    }

    public void setNumeroDePatas(int numeroDePatas) {
        this.numeroDePatas = numeroDePatas;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Mamifero{" + "nombre=" + nombre + ", temperatura=" + temperatura + ", numeroDePatas=" + numeroDePatas + ", color=" + color + '}';
    }
    
}
