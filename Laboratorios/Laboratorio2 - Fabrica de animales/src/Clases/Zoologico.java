package Clases;

import Interfaces.IAnimalFactory;
import java.util.ArrayList;

public class Zoologico {
    private String nombre;
    ArrayList<Jaula> jaulas;
    String direccion;
    String telefono;
    IAnimalFactory animalFactory;

    public Zoologico(String nombre, String direccion, String telefono, IAnimalFactory animalFactory) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.jaulas = new ArrayList<>();
        this.animalFactory = animalFactory;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Jaula> getJaulas() {
        return jaulas;
    }

    public void setJaulas(ArrayList<Jaula> jaulas) {
        this.jaulas = jaulas;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public IAnimalFactory getAnimalFactory() {
        return animalFactory;
    }

    public void setAnimalFactory(IAnimalFactory animalFactory) {
        this.animalFactory = animalFactory;
    }

    public void añadirMamifero(String nombre, double temperatura, int nroPatas, String color,
                               double altoJaula, double anchoJaula, double largoJaula) {
        Mamifero mamifero = animalFactory.crearMamifero(nombre, temperatura, nroPatas, color);
        jaulas.add(new Jaula(mamifero, altoJaula, anchoJaula, largoJaula));
    }

    public void añadirAve(String nombre, double peso, double tamañoAlas,
                           double altoJaula, double anchoJaula, double largoJaula) {
        Ave ave = animalFactory.crearAve(nombre, peso, tamañoAlas);
        jaulas.add(new Jaula(ave, altoJaula, anchoJaula, largoJaula));
    }

    public void añadirPez(String nombre, double longitud,
                           double altoJaula, double anchoJaula, double largoJaula) {
        Pez pez = animalFactory.crearPez(nombre, longitud);
        jaulas.add(new Jaula(pez, altoJaula, anchoJaula, largoJaula));
    }

    public void mostrarZoologico() {
        System.out.println("Nombre del zoológico: " + nombre);
        System.out.println("Dirección: " + direccion);
        System.out.println("Teléfono: " + telefono);
        System.out.println("Jaulas:");
        for (int i = 0; i < jaulas.size(); i++) {
            System.out.println("Jaula con" + (i + 1) + ": " + jaulas.get(i));
        }
    }

    public void mostrarMamiferos() {
        System.out.println("Mamíferos en el zoológico:");
        for (Jaula jaula : jaulas) {
            if (jaula.getAnimal() instanceof Mamifero) {
                Mamifero mamifero = (Mamifero) jaula.getAnimal();
                System.out.println(mamifero);
            }
        }
    }

    public void mostrarAves() {
        System.out.println("Aves en el zoológico:");
        for (Jaula jaula : jaulas) {
            if (jaula.getAnimal() instanceof Ave) {
                Ave ave = (Ave) jaula.getAnimal();
                System.out.println(ave);
            }
        }
    }

    public void mostrarPeces() {
        System.out.println("Peces en el zoológico:");
        for (Jaula jaula : jaulas) {
            if (jaula.getAnimal() instanceof Pez) {
                Pez pez = (Pez) jaula.getAnimal();
                System.out.println(pez);
            }
        }
    }

    @Override
    public String toString() {
        return "Zoologico{" + "nombre=" + nombre + ", jaulas=" + jaulas + ", direccion=" + direccion + ", telefono=" + telefono + ", animalFactory=" + animalFactory + '}';
    }
}
