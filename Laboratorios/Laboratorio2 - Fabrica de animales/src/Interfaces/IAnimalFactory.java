/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Interfaces;

import Clases.Ave;
import Clases.Mamifero;
import Clases.Pez;

/**
 *
 * @author david
 */
public interface IAnimalFactory {
    Mamifero crearMamifero(String nombre, double temperatura, int nroPatas, String color);
    Ave crearAve(String nombre, double peso, double tamañoAlas);
    Pez crearPez(String nombre, double longitud);
}
