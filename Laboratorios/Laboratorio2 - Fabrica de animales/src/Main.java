
import Clases.ZooAnimalFactory;
import Clases.Zoologico;
import Interfaces.IAnimalFactory;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author david
 */
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        IAnimalFactory animalFactory = new ZooAnimalFactory();
        Zoologico zoo = new Zoologico("Mi Zoológico", "Dirección del Zoológico", "123456789", animalFactory);

        Scanner scanner = new Scanner(System.in);
        int opcion;
        do {
            System.out.println("Menu:");
            System.out.println("1. Añadir Mamifero");
            System.out.println("2. Añadir Ave");
            System.out.println("3. Añadir Pez");
            System.out.println("4. Mostrar Mamiferos");
            System.out.println("5. Mostrar Aves");
            System.out.println("6. Mostrar Peces");
            System.out.println("7. Mostrar Zoológico");
            System.out.println("8. Salir");
            System.out.print("Selecciona una opción: ");
            opcion = scanner.nextInt();
            scanner.nextLine();

            switch (opcion) {
                case 1:
                    añadirMamifero(zoo, scanner);
                    break;
                case 2:
                    añadirAve(zoo, scanner);
                    break;
                case 3:
                    añadirPez(zoo, scanner);
                    break;
                case 4:
                    zoo.mostrarMamiferos();
                    break;
                case 5:
                    zoo.mostrarAves();
                    break;
                case 6:
                    zoo.mostrarPeces();
                    break;
                case 7:
                    zoo.mostrarZoologico();
                    break;
                case 8:
                    System.out.println("Saliendo del programa...");
                    break;
                default:
                    System.out.println("Opción no válida. Por favor, selecciona una opción válida.");
            }
        } while (opcion != 8);
        scanner.close();
    }

    public static void añadirMamifero(Zoologico zoo, Scanner scanner) {
        System.out.print("Nombre del mamífero: ");
        String nombre = scanner.nextLine();
        System.out.print("Temperatura corporal: ");
        double temperatura = scanner.nextDouble();
        System.out.print("Número de patas: ");
        int nroPatas = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Color: ");
        String color = scanner.nextLine();
        System.out.print("Alto de la jaula: ");
        double altoJaula = scanner.nextDouble();
        System.out.print("Ancho de la jaula: ");
        double anchoJaula = scanner.nextDouble();
        System.out.print("Largo de la jaula: ");
        double largoJaula = scanner.nextDouble();

        zoo.añadirMamifero(nombre, temperatura, nroPatas, color, altoJaula, anchoJaula, largoJaula);
    }

    public static void añadirAve(Zoologico zoo, Scanner scanner) {
        System.out.print("Nombre del ave: ");
        String nombre = scanner.nextLine();
        System.out.print("Peso: ");
        double peso = scanner.nextDouble();
        System.out.print("Tamaño de alas: ");
        double tamañoAlas = scanner.nextDouble();
        System.out.print("Alto de la jaula: ");
        double altoJaula = scanner.nextDouble();
        System.out.print("Ancho de la jaula: ");
        double anchoJaula = scanner.nextDouble();
        System.out.print("Largo de la jaula: ");
        double largoJaula = scanner.nextDouble();

        zoo.añadirAve(nombre, peso, tamañoAlas, altoJaula, anchoJaula, largoJaula);
    }

    public static void añadirPez(Zoologico zoo, Scanner scanner) {
        System.out.print("Nombre del pez: ");
        String nombre = scanner.nextLine();
        System.out.print("Longitud: ");
        double longitud = scanner.nextDouble();
        System.out.print("Alto de la jaula: ");
        double altoJaula = scanner.nextDouble();
        System.out.print("Ancho de la jaula: ");
        double anchoJaula = scanner.nextDouble();
        System.out.print("Largo de la jaula: ");
        double largoJaula = scanner.nextDouble();

        zoo.añadirPez(nombre, longitud, altoJaula, anchoJaula, largoJaula);
    }
}
